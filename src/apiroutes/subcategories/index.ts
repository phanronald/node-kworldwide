import express, { Request, Response} from 'express';
import { MongoClient, Cursor, MongoError, FilterQuery } from "mongodb";

import { IProductSubCategory as IServerProductSubCategory } from './../../models/server/products/iproductsubcategory';
import { IProductSubCategory as IClientProductSubCategory } from './../../models/client/products/iproductsubcategory';

import { CategorySubCategoryMappingService } from './../../services/category-subcat.mapping.service';

const subCategoriesRoute = express.Router()

subCategoriesRoute.get('/get-all-sub-categories', async (req: Request, res: Response) => {

	const mongoUrl = `mongodb+srv://${process.env.DB_USERNAME}:${process.env.DB_PASSWORD}@${process.env.CLUSTER_URL}/${process.env.DB_NAME}?retryWrites=true&w=majority`;
	let allProductSubCategories: IClientProductSubCategory[] = [];
	let allServerProductSubCategories: IServerProductSubCategory[] = [];

	const client = new MongoClient(mongoUrl, { useNewUrlParser: true, useUnifiedTopology: true });
	try {

		await client.connect();
		const database = client.db(`${process.env.DB_NAME}`);
		const collection = database.collection("ProductSubCategory");
		const allProductSubCategoriesCursor: Cursor = collection.find({});
 
		allProductSubCategoriesCursor.toArray((err: MongoError, result: IServerProductSubCategory[]): void => {
			if (err) {
				throw err;
			}

			allServerProductSubCategories = result;
		});

    } catch (e) {

    } finally {
        await client.close();
	}

	allProductSubCategories = await CategorySubCategoryMappingService.MapProductSubCategories(allServerProductSubCategories);

	return res.json(allProductSubCategories);
});

module.exports = subCategoriesRoute;