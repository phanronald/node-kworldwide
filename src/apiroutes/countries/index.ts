import express, { Request, Response} from 'express';
import { MongoClient, Cursor, MongoError } from "mongodb";

import { ICountry as IServerCountry } from './../../models/server/countries/icountry';
import { ICountry as IClientCountry } from './../../models/client/countries/icountry';

const countriesRouter = express.Router()

countriesRouter.get('/get-all-countries', async (req: Request, res: Response) => {
	const mongoUrl = `mongodb+srv://${process.env.DB_USERNAME}:${process.env.DB_PASSWORD}@${process.env.CLUSTER_URL}/${process.env.DB_NAME}?retryWrites=true&w=majority`;
	let allServerCountries: IServerCountry[] = [];
	let allCountries: IClientCountry[] = [];

	const client = new MongoClient(mongoUrl, { useNewUrlParser: true, useUnifiedTopology: true });
	try {

		await client.connect();

		const database = client.db(`${process.env.DB_NAME}`);
		const collection = database.collection("Country");
		const allCountriesCursor: Cursor = collection.find({});

		allCountriesCursor.toArray((err: MongoError, result: IServerCountry[]): void => {
			if (err) {
				throw err;
			}

			allServerCountries = result;
		});
 
    } catch (e) {

    } finally {
        await client.close();
	}

	allServerCountries.forEach((country: IServerCountry, index: number) => {
		
		let tempClientCountry: IClientCountry = {
			Id: country._id,
			CountryId: country.CountryId,
			Name: country.Name,
			UrlKey: country.UrlKey
		};

		allCountries.push(tempClientCountry);

	});
	
	return res.json(allCountries);
});

module.exports = countriesRouter;