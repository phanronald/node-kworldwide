import express, { Request, Response} from 'express';
import { MongoClient, Cursor, MongoError, FilterQuery } from "mongodb";

import { IProductCategory as IServerProductCategory } from './../../models/server/products/iproductcategory';
import { IProductCategory as IClientProductCategory } from './../../models/client/products/iproductcategory';

import { CategorySubCategoryMappingService } from './../../services/category-subcat.mapping.service';

import { IProductCategory } from './../../models/server/products/iproductcategory';
import { IMongoMessage } from './../../models/shared/IMongoMessage';

const categoriesRoute = express.Router()

categoriesRoute.get('/get-all-categories', async (req: Request, res: Response) => {

	const mongoUrl = `mongodb+srv://${process.env.DB_USERNAME}:${process.env.DB_PASSWORD}@${process.env.CLUSTER_URL}/${process.env.DB_NAME}?retryWrites=true&w=majority`;
	let allProductCategories: IClientProductCategory[] = [];
	let allServerProductCategories: IServerProductCategory[] = [];

	const client = new MongoClient(mongoUrl, { useNewUrlParser: true, useUnifiedTopology: true });
	try {

		await client.connect();
		const database = client.db(`${process.env.DB_NAME}`);
		const collection = database.collection("ProductCategory");
		const allProductCategoriesCursor: Cursor = collection.find({});
 
		allProductCategoriesCursor.toArray((err: MongoError, result: IServerProductCategory[]): void => {
			if (err) {
				throw err;
			}

			allServerProductCategories = result;
		});

    } catch (e) {

    } finally {
        await client.close();
	}

	allProductCategories = await CategorySubCategoryMappingService.MapProductCategories(allServerProductCategories);

	return res.json(allProductCategories);
});

categoriesRoute.get('/get-category-by-id/:id', async (req: Request, res: Response) => {

	const { params } = req;
	const { id } = params;

	let categoryId = parseInt(id, 10);
	if(Number.isNaN(categoryId)) {
		return res.json({});
	}

	const mongoUrl = `mongodb+srv://${process.env.DB_USERNAME}:${process.env.DB_PASSWORD}@${process.env.CLUSTER_URL}/${process.env.DB_NAME}?retryWrites=true&w=majority`;
	let allProductCategories: IClientProductCategory[] = [];
	let allServerProductCategories: IServerProductCategory[] = [];

	const client = new MongoClient(mongoUrl, { useNewUrlParser: true, useUnifiedTopology: true });
	try {

		await client.connect();
		const database = client.db(`${process.env.DB_NAME}`);
		const collection = database.collection("ProductCategory");

		const categoryQuery: FilterQuery<any> = { ProductCategoryId: categoryId };
		const allProductCategoriesCursor: Cursor = collection.find(categoryQuery);
 
		allProductCategoriesCursor.toArray((err: MongoError, result: IServerProductCategory[]): void => {
			if (err) {
				throw err;
			}

			allServerProductCategories = result;
		});

    } catch (e) {

    } finally {
        await client.close();
	}

	allProductCategories = await CategorySubCategoryMappingService.MapProductCategories(allServerProductCategories);
	if(allProductCategories.length > 0) {
		return res.json(allProductCategories[0]);
	}

	return res.json({});
});

categoriesRoute.post('/update-category', async (req: Request, res: Response) => {

	const productCategoryId = req.body.productCategoryId;

	let messageToReturn = {} as IMongoMessage;

	let categoryId = parseInt(productCategoryId, 10);
	if(Number.isNaN(categoryId)) {

		messageToReturn.mongoErrorMessage = "Unable to find category";
		return res.json(messageToReturn);
	}

	let postedBackCategory = { } as IProductCategory;
	if(req.body.name && /[a-zA-Z\-\s]/gm.test(req.body.name)) {
		postedBackCategory.Name = req.body.name;
	}

	if(req.body.sortOrder && /[\d]/gm.test(req.body.sortOrder)) {
		postedBackCategory.SortOrder = parseInt(req.body.sortOrder, 10);
	}

	if(req.body.navigationUrl) {
		postedBackCategory.NavigationUrl = req.body.navigationUrl;
	}

	if(req.body.imageUrl) {
		postedBackCategory.ImageUrl = req.body.imageUrl;
	}

	const mongoUrl = `mongodb+srv://${process.env.DB_USERNAME}:${process.env.DB_PASSWORD}@${process.env.CLUSTER_URL}/${process.env.DB_NAME}?retryWrites=true&w=majority`;
	const client = new MongoClient(mongoUrl, { useNewUrlParser: true, useUnifiedTopology: true });
	try {

		await client.connect();
		const database = client.db(`${process.env.DB_NAME}`);
		const collection = database.collection("ProductCategory");

		const updateCategoryQuery: FilterQuery<any> = { ProductCategoryId: categoryId };
		const setUpdateValues = { $set: postedBackCategory };
		collection.findOneAndUpdate(updateCategoryQuery, setUpdateValues, { upsert: true }, (error, response) => {

			if(error) {
				messageToReturn.mongoErrorMessage = "Unable to update provided data.";
			}

		});

    } catch (e) {

    } finally {
        await client.close();
	}

	return res.json(messageToReturn);
});

categoriesRoute.post('/add-category', async (req: Request, res: Response) => {

	const productCategoryId = req.body.productCategoryId;

	let messageToReturn = {} as IMongoMessage;

	let categoryId = parseInt(productCategoryId, 10);
	if(Number.isNaN(categoryId)) {

		messageToReturn.mongoErrorMessage = "Category Id is not valid";
		return res.json(messageToReturn);
	}

	let postedBackCategory = { } as IProductCategory;
	postedBackCategory.ProductCategoryId = categoryId;
	
	if(req.body.name && /[a-zA-Z\-\s]/gm.test(req.body.name)) {
		postedBackCategory.Name = req.body.name;
	}

	if(req.body.sortOrder && /[\d]/gm.test(req.body.sortOrder)) {
		postedBackCategory.SortOrder = parseInt(req.body.sortOrder, 10);
	}

	if(req.body.navigationUrl) {
		postedBackCategory.NavigationUrl = req.body.navigationUrl;
	}

	if(req.body.imageUrl) {
		postedBackCategory.ImageUrl = req.body.imageUrl;
	}

	const mongoUrl = `mongodb+srv://${process.env.DB_USERNAME}:${process.env.DB_PASSWORD}@${process.env.CLUSTER_URL}/${process.env.DB_NAME}?retryWrites=true&w=majority`;
	const client = new MongoClient(mongoUrl, { useNewUrlParser: true, useUnifiedTopology: true });
	try {

		await client.connect();
		const database = client.db(`${process.env.DB_NAME}`);
		const collection = database.collection("ProductCategory");

		collection.insertOne(postedBackCategory, (error, response) => {

			if(error) {
				messageToReturn.mongoErrorMessage = "Unable to create provided data.";
			}

		});

    } catch (e) {

    } finally {
        await client.close();
	}

	return res.json(messageToReturn);
});

categoriesRoute.post('/delete-category', async (req: Request, res: Response) => {

	const productCategoryId = req.body.productCategoryId;

	let messageToReturn = {} as IMongoMessage;

	let categoryId = parseInt(productCategoryId, 10);
	if(Number.isNaN(categoryId)) {

		messageToReturn.mongoErrorMessage = "Category Id is not valid";
		return res.json(messageToReturn);
	}

	
	const mongoUrl = `mongodb+srv://${process.env.DB_USERNAME}:${process.env.DB_PASSWORD}@${process.env.CLUSTER_URL}/${process.env.DB_NAME}?retryWrites=true&w=majority`;
	const client = new MongoClient(mongoUrl, { useNewUrlParser: true, useUnifiedTopology: true });
	try {

		await client.connect();
		const database = client.db(`${process.env.DB_NAME}`);
		const collection = database.collection("ProductCategory");

		const deleteCategoryQuery: FilterQuery<any> = { ProductCategoryId: categoryId };
		collection.deleteOne(deleteCategoryQuery, (error, response) => {

			if(error) {
				messageToReturn.mongoErrorMessage = "Unable to delete provided data.";
			}

		});

    } catch (e) {

    } finally {
        await client.close();
	}

	return res.json(messageToReturn);
});



module.exports = categoriesRoute;