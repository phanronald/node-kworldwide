import express, { Request, Response} from 'express';
import { MongoClient, Cursor, MongoError, FilterQuery } from "mongodb";

import { EventType } from './../../models/shared/enum/eventtype';
import { EventCategoryType } from './../../models/shared/enum/eventcategorytype';
import { IEvent as IServerEvent } from './../../models/server/events/ievent';
import { IEvent as IClientEvent } from './../../models/client/events/ievent';

const eventsRouter = express.Router()

eventsRouter.get('/get-all-events', async (req: Request, res: Response) => {

	let allEvents: IClientEvent[] = await getAllEvents(req, res);	
	return res.json(allEvents);

});

eventsRouter.get('/get-event-by-event-type/:eventType', async (req: Request, res: Response) => {

	const { params } = req;
	const { eventType } = params;

	let finalEventType = parseInt(eventType, 10);
	
	if(Number.isNaN(finalEventType) || EventType[finalEventType] === undefined) {
		return res.json([]);
	}

	const mongoUrl = `mongodb+srv://${process.env.DB_USERNAME}:${process.env.DB_PASSWORD}@${process.env.CLUSTER_URL}/${process.env.DB_NAME}?retryWrites=true&w=majority`;
	let allServerEvents: IServerEvent[] = [];

	const client = new MongoClient(mongoUrl, { useNewUrlParser: true, useUnifiedTopology: true });
	try {

		await client.connect();

		const database = client.db(`${process.env.DB_NAME}`);
		const collection = database.collection("Events");

		const eventTypeQuery: FilterQuery<any> = { EventType: finalEventType };

		const allEventsCursor: Cursor = collection.find(eventTypeQuery);
		allEventsCursor.toArray((err: MongoError, result: IServerEvent[]): void => {
			
			allServerEvents = result;
			 
		});
 
    } catch (e) {

    } finally {
        await client.close();
	}

	const allEvents = mappingEvents(allServerEvents);
	return res.json(allEvents);
});

eventsRouter.get('/get-event-by-event-category-type/:eventType/:eventCategoryType', async (req: Request, res: Response) => {

	const { params } = req;
	const { eventType, eventCategoryType } = params;

	let finalEventType = parseInt(eventType, 10);
	let finalEventCategoryType = parseInt(eventCategoryType, 10);
	
	if(Number.isNaN(finalEventType) || Number.isNaN(finalEventCategoryType) ||
		EventType[finalEventType] === undefined || 
		EventCategoryType[finalEventCategoryType] === undefined) {

		return res.json([]);
	}

	const mongoUrl = `mongodb+srv://${process.env.DB_USERNAME}:${process.env.DB_PASSWORD}@${process.env.CLUSTER_URL}/${process.env.DB_NAME}?retryWrites=true&w=majority`;
	let allServerEvents: IServerEvent[] = [];

	const client = new MongoClient(mongoUrl, { useNewUrlParser: true, useUnifiedTopology: true });
	try {

		await client.connect();

		const database = client.db(`${process.env.DB_NAME}`);
		const collection = database.collection("Events");

		const eventTypeQuery: FilterQuery<any> = { EventType: finalEventType, CategoryType: finalEventCategoryType };

		const allEventsCursor: Cursor = collection.find(eventTypeQuery);
		allEventsCursor.toArray((err: MongoError, result: IServerEvent[]): void => {
			
			allServerEvents = result;
			 
		});
 
    } catch (e) {

    } finally {
        await client.close();
	}

	const allEvents = mappingEvents(allServerEvents);
	return res.json(allEvents);
});

eventsRouter.get('/get-event-by-countryid/:countryid', async (req: Request, res: Response) => {

	const { params } = req;
	const { countryid } = params;

	let finalCountryId = parseInt(countryid, 10);
	if(Number.isNaN(finalCountryId)) {
		return res.json([]);
	}
	
	const mongoUrl = `mongodb+srv://${process.env.DB_USERNAME}:${process.env.DB_PASSWORD}@${process.env.CLUSTER_URL}/${process.env.DB_NAME}?retryWrites=true&w=majority`;
	let allServerEvents: IServerEvent[] = [];

	const client = new MongoClient(mongoUrl, { useNewUrlParser: true, useUnifiedTopology: true });
	try {

		await client.connect();

		const database = client.db(`${process.env.DB_NAME}`);
		const collection = database.collection("Events");

		const countryQuery: FilterQuery<any> = { CountryId: { $all: [finalCountryId] } };

		const allEventsCursor: Cursor = collection.find(countryQuery);
		allEventsCursor.toArray((err: MongoError, result: IServerEvent[]): void => {
			
			allServerEvents = result;
			 
		});
 
    } catch (e) {

    } finally {
        await client.close();
	}

	const allEvents = mappingEvents(allServerEvents);
	return res.json(allEvents);
});

async function getAllEvents(req: Request, res: Response): Promise<IClientEvent[]> {

	const mongoUrl = `mongodb+srv://${process.env.DB_USERNAME}:${process.env.DB_PASSWORD}@${process.env.CLUSTER_URL}/${process.env.DB_NAME}?retryWrites=true&w=majority`;
	let allServerEvents: IServerEvent[] = [];
	let allEvents: IClientEvent[] = [];

	const client = new MongoClient(mongoUrl, { useNewUrlParser: true, useUnifiedTopology: true });
	try {

		await client.connect();

		const database = client.db(`${process.env.DB_NAME}`);
		const collection = database.collection("Events");
		const allEventsCursor: Cursor = collection.find({});

		allEventsCursor.toArray((err: MongoError, result: IServerEvent[]): void => {
			
			allServerEvents = result;
			 
		});
 
    } catch (e) {

    } finally {
        await client.close();
	}

	allServerEvents.forEach((event: IServerEvent, index: number) => {
		
		let tempClientEvent: IClientEvent = {
			Id: event._id,
			Title: event.Title,
			Image: event.Image,
			Description: event.Description,
			Link: event.Link,
			EventType: event.EventType,
			EventCategoryType: event.CategoryType,
			CountryId: event.CountryId
		};

		allEvents.push(tempClientEvent);

	});

	return allEvents;

}

function mappingEvents(allServerEvents: IServerEvent[]): IClientEvent[] {

	let allEvents: IClientEvent[] = [];
	allServerEvents.forEach((event: IServerEvent, index: number) => {
		
		let tempClientEvent: IClientEvent = {
			Id: event._id,
			Title: event.Title,
			Image: event.Image,
			Description: event.Description,
			Link: event.Link,
			EventType: event.EventType,
			EventCategoryType: event.CategoryType,
			CountryId: event.CountryId
		};

		allEvents.push(tempClientEvent);

	});

	return allEvents;
}

module.exports = eventsRouter;