import express, { Request, Response} from 'express';
import { MongoClient, Cursor, MongoError, FilterQuery } from "mongodb";

import { IColor as IServerColor } from './../../models/server/colors/icolor';
import { IColor as IClientColor } from './../../models/client/colors/icolor';

const colorsRoute = express.Router()

colorsRoute.get('/get-all-colors', async (req: Request, res: Response) => {

	const mongoUrl = `mongodb+srv://${process.env.DB_USERNAME}:${process.env.DB_PASSWORD}@${process.env.CLUSTER_URL}/${process.env.DB_NAME}?retryWrites=true&w=majority`;
	let allColors: IClientColor[] = [];
	let allServerColors: IServerColor[] = [];

	const client = new MongoClient(mongoUrl, { useNewUrlParser: true, useUnifiedTopology: true });
	try {

		await client.connect();

		// Establish and verify connection
		//await client.db(`${process.env.DB_NAME}`).command({ ping: 1 });
		const database = client.db(`${process.env.DB_NAME}`);
		const collection = database.collection("Colors");
		const allColorsCursor: Cursor = collection.find({});
		
		
		allColorsCursor.toArray((err: MongoError, result: IServerColor[]): void => {
			if (err) {
				throw err;
			}

			allServerColors = result;
		});
 
    } catch (e) {

    } finally {
        await client.close();
	}
	
	allServerColors.forEach((color: IServerColor, index: number) => {

		let tempClientColor: IClientColor = {
			Id: color._id,
			ColorId: color.ColorId,
			ColorName: color.ColorName,
			ImageUrl: color.ImageUrl
		};

		allColors.push(tempClientColor);

	});

	return res.json(allColors);
});

colorsRoute.get('/get-color-by-id/:id', async (req: Request, res: Response) => {

	const { params } = req;
	const { id } = params;

	let colorId = parseInt(id, 10);
	if(Number.isNaN(colorId)) {
		return res.json({});
	}

	const mongoUrl = `mongodb+srv://${process.env.DB_USERNAME}:${process.env.DB_PASSWORD}@${process.env.CLUSTER_URL}/${process.env.DB_NAME}?retryWrites=true&w=majority`;
	let allColors: IClientColor[] = [];
	let allServerColors: IServerColor[] = [];

	const client = new MongoClient(mongoUrl, { useNewUrlParser: true, useUnifiedTopology: true });
	try {

		await client.connect();
		const database = client.db(`${process.env.DB_NAME}`);
		const collection = database.collection("Colors");

		const colorQuery: FilterQuery<any> = { ColorId: colorId };
		const allColorsCursor: Cursor = collection.find(colorQuery);
 
		allColorsCursor.toArray((err: MongoError, result: IServerColor[]): void => {
			if (err) {
				throw err;
			}

			allServerColors = result;
		});

    } catch (e) {

    } finally {
        await client.close();
	}

	allServerColors.forEach((color: IServerColor, index: number) => {

		let tempClientColor: IClientColor = {
			Id: color._id,
			ColorId: color.ColorId,
			ColorName: color.ColorName,
			ImageUrl: color.ImageUrl
		};

		allColors.push(tempClientColor);

	});

	if(allColors.length > 0) {
		return res.json(allColors[0]);
	}

	return res.json({});
});

module.exports = colorsRoute;