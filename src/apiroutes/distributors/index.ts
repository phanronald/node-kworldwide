import express, { Request, Response} from 'express';
import { MongoClient, Cursor, MongoError, FilterQuery } from "mongodb";

import { IDistributor as IServerDistributor } from './../../models/server/distributors/idistributor';
import { IDistributor as IClientDistributor } from './../../models/client/distributors/idistributor';

const distributorsRouter = express.Router()

distributorsRouter.get('/get-distributors-dealer', async (req: Request, res: Response) => {

	const mongoUrl = `mongodb+srv://${process.env.DB_USERNAME}:${process.env.DB_PASSWORD}@${process.env.CLUSTER_URL}/${process.env.DB_NAME}?retryWrites=true&w=majority`;
	let allDistributors: IClientDistributor[] = [];
	let allServerDistributors: IServerDistributor[] = [];

	const client = new MongoClient(mongoUrl, { useNewUrlParser: true, useUnifiedTopology: true });
	try {

		await client.connect();
		const database = client.db(`${process.env.DB_NAME}`);
		const collection = database.collection("Distributor");
		const allColorsCursor: Cursor = collection.find({});
		
		
		allColorsCursor.toArray((err: MongoError, result: IServerDistributor[]): void => {
			if (err) {
				throw err;
			}

			allServerDistributors = result;
		});
 
    } catch (e) {

    } finally {
        await client.close();
	}

	allServerDistributors.forEach((distributor: IServerDistributor, index: number) => {

		let tempDistributor: IClientDistributor = {
			Id: distributor._id,
			DistributorId: distributor.DistributorId,
			DistributorSite: distributor.DistributorSite,
			MapImage: distributor.MapImage,
			CountryId: distributor.CountryId,
			Name: distributor.Name,
			Email: distributor.Email,
			Phone: distributor.Phone,
			Fax: distributor.Fax,
			Address: distributor.Address,
			Website: distributor.Website,
			Latitude: distributor.Latitude,
			Longitude: distributor.Longitude,
			SocialMedia: distributor.SocialMedia
		};

		allDistributors.push(tempDistributor);

	});

	return res.json(allDistributors);

});

module.exports = distributorsRouter;