import express, { Request, Response} from 'express';
import { MongoClient, Cursor, MongoError } from "mongodb";

import { IAbout as IServerAbout } from './../../models/server/about/iabout';
import { IAbout as IClientAbout } from './../../models/client/about/iabout';

const aboutRoute = express.Router()

aboutRoute.get('/get-about', async (req: Request, res: Response) => {

	const mongoUrl = `mongodb+srv://${process.env.DB_USERNAME}:${process.env.DB_PASSWORD}@${process.env.CLUSTER_URL}/${process.env.DB_NAME}?retryWrites=true&w=majority`;
	let allAbout: IClientAbout[] = [];
	let allServerAbout: IServerAbout[] = [];

	const client = new MongoClient(mongoUrl, { useNewUrlParser: true, useUnifiedTopology: true });
	try {

		await client.connect();

		// Establish and verify connection
		//await client.db(`${process.env.DB_NAME}`).command({ ping: 1 });
		const database = client.db(`${process.env.DB_NAME}`);
		const collection = database.collection("AboutPage");
		const allAboutCursor: Cursor = collection.find({});

		allAboutCursor.toArray((err: MongoError, result: IServerAbout[]): void => {
			if (err) {
				throw err;
			}

			allServerAbout = result;
		});
 
    } catch (e) {

    } finally {
        await client.close();
	}

	allServerAbout.forEach((about: IServerAbout, index: number) => {

		let tempClientAbout: IClientAbout = {
			Id: about._id,
			HeaderText: about.HeaderText,
			HeaderImage: about.HeaderImage,
			Overview: about.Overview,
			Detailed: about.Detailed,
			MainAddress: about.MainAddress,
			MailAddress: about.MailAddress,
			Phone: about.Phone,
			AboutLinks: about.AboutLinks,
			Contact: about.Contact
		};

		allAbout.push(tempClientAbout);

	});

	return res.json(allAbout);
});

module.exports = aboutRoute;