import express, { Request, Response} from 'express';
import { MongoClient, Cursor, MongoError, FilterQuery } from "mongodb";

import { IProductCategory as IServerProductCategory } from './../../models/server/products/iproductcategory';
import { IProductCategory as IClientProductCategory } from './../../models/client/products/iproductcategory';
import { IProductSubCategory as IServerProductSubCategory } from './../../models/server/products/iproductsubcategory';
import { IProductSubCategory as IClientProductSubCategory } from './../../models/client/products/iproductsubcategory';
import { IProduct as IServerProduct } from './../../models/server/products/iproduct';
import { IProduct as IClientProduct } from './../../models/client/products/iproduct';
import { IProductVideo as IServerProductVideo } from './../../models/server/products/iproductvideo';
import { IProductVideo as IClientProductVideo } from './../../models/client/products/iproductvideo';
import { IProductPhoto as IServerProductPhoto } from './../../models/server/products/iproductphoto';
import { IProductPhoto as IClientProductPhoto } from './../../models/client/products/iproductphoto';
import { IProductSpecification as IProductClientSpecification } from './../../models/client/products/iproductspecification';
import { IProductSpecGroupWithSpecs } from './../../models/viewmodel/iproductspecgroupwithspecs';
import { IVmProductSpecification } from './../../models/viewmodel/ivmproductspecification';
import { Collection } from './../../shared/core/collection';

import { ProductMappingService } from './../../services/product.mapping.service';
import { ProductService } from './../../services/product.service';

const productRoute = express.Router()

productRoute.get('/get-all-products', async (req: Request, res: Response) => {

	const mongoUrl = `mongodb+srv://${process.env.DB_USERNAME}:${process.env.DB_PASSWORD}@${process.env.CLUSTER_URL}/${process.env.DB_NAME}?retryWrites=true&w=majority`;
	let allProducts: IClientProduct[] = [];
	let allServerProducts: IServerProduct[] = [];

	const client = new MongoClient(mongoUrl, { useNewUrlParser: true, useUnifiedTopology: true });
	try {

		await client.connect();
		const database = client.db(`${process.env.DB_NAME}`);
		const collection = database.collection("Product");
		const allProductCategoriesCursor: Cursor = collection.find({});
 
		allProductCategoriesCursor.toArray((err: MongoError, result: IServerProduct[]): void => {
			if (err) {
				throw err;
			}

			allServerProducts = result;
		});

    } catch (e) {

    } finally {
        await client.close();
	}

	allProducts = await ProductMappingService.MapProducts(allServerProducts);

	return res.json(allProducts);
});

productRoute.get('/get-product-info/:id', async (req: Request, res: Response) => {

	const { params } = req;
	const { id } = params;

	let finalProductId = parseInt(id, 10);
	if(Number.isNaN(finalProductId)) {
		return res.json([]);
	}
	
	const mongoUrl = `mongodb+srv://${process.env.DB_USERNAME}:${process.env.DB_PASSWORD}@${process.env.CLUSTER_URL}/${process.env.DB_NAME}?retryWrites=true&w=majority`;
	let allProducts: IClientProduct[] = [];
	let allServerProducts: IServerProduct[] = [];

	const client = new MongoClient(mongoUrl, { useNewUrlParser: true, useUnifiedTopology: true });
	try {

		await client.connect();
		const database = client.db(`${process.env.DB_NAME}`);
		const collection = database.collection("Product");

		const productQuery: FilterQuery<any> = { ProductId: finalProductId };
		const allProductsCursor: Cursor = collection.find(productQuery);
 
		allProductsCursor.toArray((err: MongoError, result: IServerProduct[]): void => {
			if (err) {
				throw err;
			}

			allServerProducts = result;
		});

    } catch (e) {

    } finally {
        await client.close();
	}

	allProducts = await ProductMappingService.MapProducts(allServerProducts);

	return res.json(allProducts);
});

productRoute.get('/get-product-video/:productid', async (req: Request, res: Response) => {

	const { params } = req;
	const { productid } = params;

	let finalProductId = parseInt(productid, 10);
	if(Number.isNaN(finalProductId)) {
		return res.json([]);
	}
	
	const mongoUrl = `mongodb+srv://${process.env.DB_USERNAME}:${process.env.DB_PASSWORD}@${process.env.CLUSTER_URL}/${process.env.DB_NAME}?retryWrites=true&w=majority`;
	let allProductVideos: IClientProductVideo[] = [];
	let allServerProductVideos: IServerProductVideo[] = [];

	const client = new MongoClient(mongoUrl, { useNewUrlParser: true, useUnifiedTopology: true });
	try {

		await client.connect();
		const database = client.db(`${process.env.DB_NAME}`);
		const collection = database.collection("ProductVideos");

		const productQuery: FilterQuery<any> = { ProductId: finalProductId };
		const allProductVideosCursor: Cursor = collection.find(productQuery);
 
		allProductVideosCursor.toArray((err: MongoError, result: IServerProductVideo[]): void => {
			if (err) {
				throw err;
			}

			allServerProductVideos = result;
		});

    } catch (e) {

    } finally {
        await client.close();
	}

	allProductVideos = await ProductMappingService.MapProductVideos(allServerProductVideos);

	return res.json(allProductVideos);

});

productRoute.get('/get-product-photos/:productid', async (req: Request, res: Response) => {

	const { params } = req;
	const { productid } = params;

	let finalProductId = parseInt(productid, 10);
	if(Number.isNaN(finalProductId)) {
		return res.json([]);
	}
	
	const mongoUrl = `mongodb+srv://${process.env.DB_USERNAME}:${process.env.DB_PASSWORD}@${process.env.CLUSTER_URL}/${process.env.DB_NAME}?retryWrites=true&w=majority`;
	let allProductPhotos: IClientProductPhoto[] = [];
	let allServerProductPhotos: IServerProductPhoto[] = [];

	const client = new MongoClient(mongoUrl, { useNewUrlParser: true, useUnifiedTopology: true });
	try {

		await client.connect();
		const database = client.db(`${process.env.DB_NAME}`);
		const collection = database.collection("ProductPhotos");

		const productQuery: FilterQuery<any> = { ProductId: finalProductId };
		const allProductPhotosCursor: Cursor = collection.find(productQuery);
 
		allProductPhotosCursor.toArray((err: MongoError, result: IServerProductPhoto[]): void => {
			if (err) {
				throw err;
			}

			allServerProductPhotos = result;
		});

    } catch (e) {

    } finally {
        await client.close();
	}

	allProductPhotos = await ProductMappingService.MapProductPhotos(allServerProductPhotos);

	return res.json(allProductPhotos);

});

productRoute.get('/get-product-specifications/:productid', async (req: Request, res: Response) => {

	const { params } = req;
	const { productid } = params;

	let finalProductId = parseInt(productid, 10);
	if(Number.isNaN(finalProductId)) {
		return res.json([]);
	}
	
	const model = ProductService.GetAllProductInfos();
	return res.json(model);

});



module.exports = productRoute;