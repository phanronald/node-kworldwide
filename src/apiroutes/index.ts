import express, { Request, Response} from 'express';
import * as path from 'path';
import { DirectoryService } from './../services/directory-service';

const router = express.Router()


router.get('/', function (req: Request, res: Response) {
	return res.send('API is working 🤓 ' + process.env.WHO_AM_I);
});

router.post('/save-image', (req: Request, res: Response) => {
	const image = req.files.myFile;
	console.log(req.body.firstName, "First NAME");
	console.log(req.body.lastName, "Last NAME");
	const imagePath = path.join(__dirname, '../../images/' + image.name);

	DirectoryService.CreateDirectory(path.join(__dirname, '../../images/'));

	image.mv(imagePath, (error) => {

		if (error) {
			console.error(error);

			res.writeHead(500, {
				'Content-Type': 'application/json'
			});

			res.end(JSON.stringify({ status: 'error', message: error }))
			return;
		}

		res.writeHead(200, {
			'Content-Type': 'application/json'
		});

		res.end(JSON.stringify({ status: 'success', path: '/images/' + image.name }))
	})
});

router.use("/api/Categories", require('./categories/index'));
router.use("/api/SubCategories", require('./subCategories/index'));
router.use("/api/About", require('./about/index'));
router.use("/api/Colors", require('./colors/index'));
router.use("/api/Country", require('./countries/index'));
router.use("/api/Events", require('./events/index'));
router.use("/api/Product", require('./products/index'));
router.use("/api/Distributors", require('./distributors/index'));
// router.get('/', rootHandler);
// router.get('/hello/:name', helloHandler);

// GET /
// router.get('/', function (req, res) {

// });

// // GET /countries
// router.get('/countries', (req, res, next) => {

// });

module.exports = router;