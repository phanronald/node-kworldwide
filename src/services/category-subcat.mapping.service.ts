
import { IProductCategory as IServerProductCategory } from './../models/server/products/iproductcategory';
import { IProductCategory as IClientProductCategory } from './../models/client/products/iproductcategory';
import { IProductSubCategory as IServerProductSubCategory } from './../models/server/products/iproductsubcategory';
import { IProductSubCategory as IClientProductSubCategory } from './../models/client/products/iproductsubcategory';

export class CategorySubCategoryMappingService {

	public static MapProductCategories = async (allServerProductCategories: IServerProductCategory[]): Promise<IClientProductCategory[]> => {

		let allProductCategories: IClientProductCategory[] = [];
		allServerProductCategories.forEach((productCategory: IServerProductCategory, index: number) => {

			let tempClientProductCategory: IClientProductCategory = {
				Id: productCategory._id,
				ProductCategoryId: productCategory.ProductCategoryId,
				Name: productCategory.Name,
				SortOrder: productCategory.SortOrder,
				NavigationUrl: productCategory.NavigationUrl,
				ImageUrl: productCategory.ImageUrl
			};
	
			allProductCategories.push(tempClientProductCategory);
	
		});

		return allProductCategories;
	}

	public static MapProductSubCategories = async (allServerProductSubCategories: IServerProductSubCategory[]): Promise<IClientProductSubCategory[]> => {

		let allProductSubCategories: IClientProductSubCategory[] = [];
		allServerProductSubCategories.forEach((productSubCategory: IServerProductSubCategory, index: number) => {

			let tempClientProductSubCategory: IClientProductSubCategory = {
				Id: productSubCategory._id,
				ProductSubCategoryId: productSubCategory.ProductSubCategoryId,
				ProductCategoryId: productSubCategory.ProductCategoryId,
				Name: productSubCategory.Name,
				IsActive: productSubCategory.IsActive,
				SortOrder: productSubCategory.SortOrder,
			};
	
			allProductSubCategories.push(tempClientProductSubCategory);
	
		});

		return allProductSubCategories;
	}

}