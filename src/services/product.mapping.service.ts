
import { IProduct as IServerProduct } from './../models/server/products/iproduct';
import { IProduct as IClientProduct } from './../models/client/products/iproduct';
import { IProductVideo as IServerProductVideo } from './../models/server/products/iproductvideo';
import { IProductVideo as IClientProductVideo } from './../models/client/products/iproductvideo';
import { IProductPhoto as IServerProductPhoto } from './../models/server/products/iproductphoto';
import { IProductPhoto as IClientProductPhoto } from './../models/client/products/iproductphoto';
import { IProductSpecification as IServerProductSpecification } from './../models/server/products/iproductspecification';
import { IProductSpecification as IClientProductSpecification } from './../models/client/products/iproductspecification';
import { ISpecification as IServerSpecification } from './../models/server/products/ispecification';
import { ISpecification as IClientSpecification } from './../models/client/products/ispecification';
import { ISpecificationGroup as IServerSpecificationGroup } from './../models/server/products/ispecificationgroup';
import { ISpecificationGroup as IClientSpecificationGroup } from './../models/client/products/ispecificationgroup';

export class ProductMappingService {

	public static MapProducts = async (allServerProducts: IServerProduct[]): Promise<IClientProduct[]> => {

		let allProducts: IClientProduct[] = [];
		allServerProducts.forEach((product: IServerProduct, index: number) => {

			let tempClientProduct: IClientProduct = {
				Id: product._id,
				ProductId: product.ProductId,
				CategoryId: product.CategoryId,
				SubCategoryId: product.SubCategoryId,
				Name: product.Name,
				MSRP: product.MSRP,
				Year: product.Year,
				UrlKey: product.UrlKey,
				IsActive: product.IsActive,
				IsNavigation: product.IsNavigation,
				NavImageUrl: product.NavImageUrl,
				SortOrder: product.SortOrder
			};
	
			allProducts.push(tempClientProduct);
	
		});

		return allProducts;
	}

	public static MapProductVideos = async (allServerProductVideos: IServerProductVideo[]): Promise<IClientProductVideo[]> => {

		let allProductVideos: IClientProductVideo[] = [];
		allServerProductVideos.forEach((productVideo: IServerProductVideo, index: number) => {

			let tempClientProductVideo: IClientProductVideo = {
				Id: productVideo._id,
				ProductId: productVideo.ProductId,
				FileName: productVideo.FileName,
				ThumbnailName: productVideo.ThumbnailName,
				Title: productVideo.Title
			};
	
			allProductVideos.push(tempClientProductVideo);
	
		});

		return allProductVideos;
	}

	public static MapProductPhotos = async (allServerProductPhotos: IServerProductPhoto[]): Promise<IClientProductPhoto[]> => {

		let allProductPhotos: IClientProductPhoto[] = [];
		allServerProductPhotos.forEach((productVideo: IServerProductPhoto, index: number) => {

			let tempClientProductPhoto: IClientProductPhoto = {
				Id: productVideo._id,
				ProductId: productVideo.ProductId,
				FileName: productVideo.FileName,
				Title: productVideo.Title
			};
	
			allProductPhotos.push(tempClientProductPhoto);
	
		});

		return allProductPhotos;
	}

	public static MapProductSpecs = async (allServerProductSpecs: IServerProductSpecification[]): Promise<IClientProductSpecification[]> => {

		let allProductSpecs: IClientProductSpecification[] = [];
		allServerProductSpecs.forEach((productSpec: IServerProductSpecification, index: number) => {

			let tempClientProductSpec: IClientProductSpecification = {
				Id: productSpec._id,
				ProductSpecId: productSpec.ProductSpecId,
				SpecId: productSpec.SpecId,
				ProductId: productSpec.ProductId,
				SpecValue: productSpec.SpecValue,
				IsFeatured: productSpec.IsFeatured
			};
	
			allProductSpecs.push(tempClientProductSpec);
	
		});

		return allProductSpecs;
	}

	public static MapSpecifications = async (allServerSpecs: IServerSpecification[]): Promise<IClientSpecification[]> => {

		let allSpecs: IClientSpecification[] = [];
		allServerSpecs.forEach((spec: IServerSpecification, index: number) => {

			let tempClientProductSpec: IClientSpecification = {
				Id: spec._id,
				ProductSpecId: spec.ProductSpecId,
				SpecGroupId: spec.SpecGroupId,
				Name: spec.Name,
				SortOrder: spec.SortOrder,
				IsDisplayed: spec.IsDisplayed
			};
	
			allSpecs.push(tempClientProductSpec);
	
		});

		return allSpecs;
	}

	public static MapSpecificationGroup = async (allServerSpecGroups: IServerSpecificationGroup[]): Promise<IClientSpecificationGroup[]> => {

		let allSpecGroups: IClientSpecificationGroup[] = [];
		allServerSpecGroups.forEach((specGroup: IServerSpecificationGroup, index: number) => {

			let tempClientProductSpec: IClientSpecificationGroup = {
				Id: specGroup._id,
				SpecGroupId: specGroup.SpecGroupId,
				Name: specGroup.Name,
				SortOrder: specGroup.SortOrder
			};
	
			allSpecGroups.push(tempClientProductSpec);
	
		});

		return allSpecGroups;
	}

}