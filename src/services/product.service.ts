
import { MongoClient, Cursor, MongoError, FilterQuery } from "mongodb";

import { Collection } from './../shared/core/collection';

import { ISpecification as IServerSpecification } from './../models/server/products/ispecification';
import { ISpecification as IClientSpecification } from './../models/client/products/ispecification';
import { ISpecificationGroup as IServerSpecificationGroup } from './../models/server/products/ispecificationgroup';
import { ISpecificationGroup as IClientSpecificationGroup } from './../models/client/products/ispecificationgroup';
import { IProductSpecification as IServerProductSpecification } from './../models/server/products/iproductspecification';
import { IProductSpecification as IClientProductSpecification } from './../models/client/products/iproductspecification';
import { IProductSpecGroupWithSpecs } from './../models/viewmodel/iproductspecgroupwithspecs';
import { IVmProductSpecification } from './../models/viewmodel/ivmproductspecification';

import { ProductMappingService } from './product.mapping.service';

export class ProductService {

	public static GetAllProductSpecifications = async (): Promise<IClientProductSpecification[]> => {

		const mongoUrl = `mongodb+srv://${process.env.DB_USERNAME}:${process.env.DB_PASSWORD}@${process.env.CLUSTER_URL}/${process.env.DB_NAME}?retryWrites=true&w=majority`;
		let allProductSpecs: IClientProductSpecification[] = [];
		let allServerProductSpecs: IServerProductSpecification[] = [];

		const client = new MongoClient(mongoUrl, { useNewUrlParser: true, useUnifiedTopology: true });
		try {

			await client.connect();
			const database = client.db(`${process.env.DB_NAME}`);
			const collection = database.collection("ProductSpecifiation");

			const allProductSpecsCursor: Cursor = collection.find({});

			allProductSpecsCursor.toArray((err: MongoError, result: IServerProductSpecification[]): void => {
				if (err) {
					throw err;
				}

				allServerProductSpecs = result;
			});

		} catch (e) {

		} finally {
			await client.close();
		}

		allProductSpecs = await ProductMappingService.MapProductSpecs(allServerProductSpecs);

		return allProductSpecs;
	}

	public static GetAllSpecifications = async (): Promise<IClientSpecification[]> => {

		const mongoUrl = `mongodb+srv://${process.env.DB_USERNAME}:${process.env.DB_PASSWORD}@${process.env.CLUSTER_URL}/${process.env.DB_NAME}?retryWrites=true&w=majority`;
		let allSpecs: IClientSpecification[] = [];
		let allServerSpecs: IServerSpecification[] = [];

		const client = new MongoClient(mongoUrl, { useNewUrlParser: true, useUnifiedTopology: true });
		try {

			await client.connect();
			const database = client.db(`${process.env.DB_NAME}`);
			const collection = database.collection("Specification");

			const allSpecsCursor: Cursor = collection.find({});

			allSpecsCursor.toArray((err: MongoError, result: IServerSpecification[]): void => {
				if (err) {
					throw err;
				}

				allServerSpecs = result;
			});

		} catch (e) {

		} finally {
			await client.close();
		}

		allSpecs = await ProductMappingService.MapSpecifications(allServerSpecs);

		return allSpecs;
	}

	public static GetAllSpecificationGroups = async (): Promise<IClientSpecificationGroup[]> => {

		const mongoUrl = `mongodb+srv://${process.env.DB_USERNAME}:${process.env.DB_PASSWORD}@${process.env.CLUSTER_URL}/${process.env.DB_NAME}?retryWrites=true&w=majority`;
		let allSpecGroups: IClientSpecificationGroup[] = [];
		let allServerSpecGroups: IServerSpecificationGroup[] = [];

		const client = new MongoClient(mongoUrl, { useNewUrlParser: true, useUnifiedTopology: true });
		try {

			await client.connect();
			const database = client.db(`${process.env.DB_NAME}`);
			const collection = database.collection("SpecificationGroup");

			const allSpecGroupsCursor: Cursor = collection.find({});

			allSpecGroupsCursor.toArray((err: MongoError, result: IServerSpecificationGroup[]): void => {
				if (err) {
					throw err;
				}

				allServerSpecGroups = result;
			});

		} catch (e) {

		} finally {
			await client.close();
		}

		allSpecGroups = await ProductMappingService.MapSpecificationGroup(allServerSpecGroups);

		return allSpecGroups;
	}

	public static GetAllProductInfos = async (): Promise<IVmProductSpecification> => {

		
		let allServerSpecs: IServerSpecification[] = [];
		let allServerProductSpecs: IServerProductSpecification[] = [];
		let allServerSpecGroups: IServerSpecificationGroup[] = [];

		let allSpecs: Collection<IClientSpecification> = new Collection<IClientSpecification>();
		let allProductSpecs: Collection<IClientProductSpecification> = new Collection<IClientProductSpecification>();
		let allSpecGroups: Collection<IClientSpecificationGroup> = new Collection<IClientSpecificationGroup>();
		

		const mongoUrl = `mongodb+srv://${process.env.DB_USERNAME}:${process.env.DB_PASSWORD}@${process.env.CLUSTER_URL}/${process.env.DB_NAME}?retryWrites=true&w=majority`;
		const client = new MongoClient(mongoUrl, { useNewUrlParser: true, useUnifiedTopology: true });
		try {

			await client.connect();
			const database = client.db(`${process.env.DB_NAME}`);
			const specCollection = database.collection("Specification");
			//const productSpecCollection = database.collection("ProductSpecifiation");
			//const specGroupCollection = database.collection("SpecificationGroup");

			const allSpecsCursor: Cursor = specCollection.find({});
			//const allProductSpecsCursor: Cursor = productSpecCollection.find({});
			//const allSpecGroupsCursor: Cursor = specGroupCollection.find({});

			allSpecsCursor.toArray((err: MongoError, result: IServerSpecification[]): void => {
				if (err) {
					throw err;
				}

				allServerSpecs = result;
			});

			// allProductSpecsCursor.toArray((err: MongoError, result: IServerProductSpecification[]): void => {
			// 	if (err) {
			// 		throw err;
			// 	}

			// 	allServerProductSpecs = result;
			// });

			// allSpecGroupsCursor.toArray((err: MongoError, result: IServerSpecificationGroup[]): void => {
			// 	if (err) {
			// 		throw err;
			// 	}

			// 	allServerSpecGroups = result;
			// });


		} catch (e) {

		} finally {
			await client.close();
		}

		allSpecs = new Collection(await ProductMappingService.MapSpecifications(allServerSpecs));
		// allProductSpecs = new Collection(await ProductMappingService.MapProductSpecs(allServerProductSpecs));
		// allSpecGroups = new Collection(await ProductMappingService.MapSpecificationGroup(allServerSpecGroups));

		// const allSpecIdsInProduct = allProductSpecs.Select(x => x.SpecId);
		// const allSpecGroupIds = allSpecs.Where(x => allSpecIdsInProduct.Contains(x.ProductSpecId))
		// 	.Select(x => x.SpecGroupId);

		// const allSpecGroupWithId = allSpecGroups.Where(x => allSpecGroupIds.Contains(x.SpecGroupId));
		// let listProductSpecGroupWithSpecs = new Collection<IProductSpecGroupWithSpecs>();
		// allProductSpecs.ForEach((productSpec: IClientProductSpecification, index: number) => {

		// 	const foundSpec = allSpecs.Where(x => x.ProductSpecId == productSpec.SpecId).FirstOrDefault();
		// 	const foundSpecGroup = allSpecGroups.Where(x => x.SpecGroupId == foundSpec.SpecGroupId).FirstOrDefault();
		// 	var tProdSpecGroupWithSpec: IProductSpecGroupWithSpecs = {
		// 		SpecGroupId: foundSpecGroup.SpecGroupId,
		// 		SpecName: foundSpec.Name,
		// 		SpecValue: productSpec.SpecValue
		// 	};

		// 	listProductSpecGroupWithSpecs.Add(tProdSpecGroupWithSpec);

		// });

		// const model: IVmProductSpecification = {
		// 	ProductSpecGroupWithSpecs: listProductSpecGroupWithSpecs,
		// 	SpecificationGroups: allSpecGroups
		// };

		//return model;
		console.log(allSpecs.ToArray(), "AAAA");
		return null;
	}

}