import express, { Request, Response, NextFunction } from 'express';
import * as bodyParser from "body-parser";
import fileupload from 'express-fileupload';

class App {

	public app: express.Application;
	public mongoUrl: string = `mongodb+srv://${process.env.DB_USERNAME}:${process.env.DB_PASSWORD}@${process.env.CLUSTER_URL}/${process.env.DB_NAME}?retryWrites=true&w=majority`;

	constructor() {
		this.app = express();
		this.config();

		this.app.use('/', require('./../apiroutes/index'));
	}

	private config(): void {

		this.app.use((req: Request, res: Response, next: NextFunction) => {
			res.header("Access-Control-Allow-Origin", "*");
			res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
			res.header('Access-Control-Allow-Methods', 'PUT, POST, GET, DELETE, OPTIONS');
			next();
		});

		//support application/x-www-form-urlencoded post data
		this.app.use(bodyParser.urlencoded({ extended: false }));
		// support application/json type post data
		this.app.use(bodyParser.json());

		this.app.use(bodyParser.json({
			limit: '50mb',
			verify(req: any, res, buf, encoding) {
				req.rawBody = buf;
			}
		}));

		this.app.use(fileupload({
			limits: { fileSize: 5 * 1024 * 1024 },
			useTempFiles : true,
			tempFileDir : '/tmp/'
		}));
	}
}

export default new App().app;