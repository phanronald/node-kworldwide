
import * as dotenv from "dotenv";
import path from 'path';

import app from "./clientapp/app";

dotenv.config();
//dotenv.config({ path: path.resolve(__dirname, `./${process.env.ENVIRONMENT}.env`)});
//dotenv.config({path: `src/environments/.env.${process.env.NODE_ENV}`});

const port = process.env.PORT;
app.listen(port).on('error', (err: Error) => {
	if (err) {
		return console.error(err);
	}
	return console.log(`server is listening on ${port}`);
});

// import express from 'express';
// import * as bodyParser from 'body-parser';

// const app = express();
// const port = process.env.PORT || '8000';

// app.use(bodyParser.json({
//     limit: '50mb',
//     verify(req: any, res, buf, encoding) {
//         req.rawBody = buf;
//     }
// }));

// app.use('/', require('./routes/index'))

// app.listen(port).on('error', (err: Error) => {
// 	if (err) {
// 		return console.error(err);
// 	}
// 	return console.log(`server is listening on ${port}`);
// });