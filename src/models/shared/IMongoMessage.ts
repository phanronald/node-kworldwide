
export interface IMongoMessage {
	mongoErrorMessage: string;
	mongoMessage: string;
}