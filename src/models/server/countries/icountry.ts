export interface ICountry {
	_id: string;
	CountryId: number;
	Name: string;
	UrlKey: string;
}