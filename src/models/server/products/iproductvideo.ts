
export interface IProductVideo {
	_id: string;
	ProductId: number;
	FileName: string;
	ThumbnailName: string;
	Title: string;
}
