

export interface IProduct {
	_id: string;
	ProductId: number;
	CategoryId: number;
	SubCategoryId: number;
	Name: string;
	MSRP: number;
	Year: number;
	UrlKey: string;
	IsActive: boolean;
	IsNavigation: boolean;
	NavImageUrl: string;
	SortOrder: number;
}
