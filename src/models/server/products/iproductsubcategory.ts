

export interface IProductSubCategory {
	_id: string;
	ProductSubCategoryId: number;
	ProductCategoryId: number;
	Name: string;
	IsActive: boolean;
	SortOrder: number;
}
