
export interface IProductPhoto {
	_id: string;
	ProductId: number;
	FileName: string;
	Title: string;
}
