export interface ISpecificationGroup {
	_id: string;
	SpecGroupId: number;
	Name: string;
	SortOrder: number;
}