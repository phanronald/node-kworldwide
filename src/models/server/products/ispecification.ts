
export interface ISpecification {
	_id: string;
	ProductSpecId: number;
	SpecGroupId: number;
	Name: string;
	SortOrder: number;
	IsDisplayed: boolean;
}
