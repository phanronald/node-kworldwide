

export interface IProductColorPhoto {
	_id: string;
	ProductColorPhotoId: number;
	ProductId: number;
	ColorId: number;
	ImageUrl: string;
}
