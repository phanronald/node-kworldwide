

export interface IProductCategory {
	_id: string;
	ProductCategoryId: number;
	Name: string;
	SortOrder: number;
	NavigationUrl: string;
	ImageUrl: string;
}
