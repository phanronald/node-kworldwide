
export interface IProductSpecification {
	_id: string;
	ProductSpecId: number;
	ProductId: number;
	SpecId: number;
	SpecValue: string;
	IsFeatured: boolean;
}
