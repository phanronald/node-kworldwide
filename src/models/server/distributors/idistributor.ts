
import { IAddressInfo } from './../../shared/IAddressInfo';
import { ISocialMedia } from './../../shared/ISocialMedia';

export interface IDistributor
{
	_id: string;
	DistributorId: number;
	DistributorSite: string;
	MapImage: string;

	CountryId: number;
	Name: string;
	Email: string;
	Phone: string;
	Fax: string;
	Address: IAddressInfo;
	Website: string;
	Latitude: string;
	Longitude: string;
	SocialMedia: ISocialMedia;
}