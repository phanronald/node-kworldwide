
import { EventType } from './../../shared/enum/eventtype';
import { EventCategoryType } from './../../shared/enum/eventcategorytype';

export interface IEvent {
	_id: string;
	Image: string;
	Title: string;
	Description: string;
	Link: string;
	EventType: EventType;
	CategoryType: EventCategoryType;
	CountryId: number[];
}