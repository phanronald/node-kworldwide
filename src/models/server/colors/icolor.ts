export interface IColor {
	_id: string;
	ColorId: number;
	ColorName: string;
	ImageUrl: string;
}