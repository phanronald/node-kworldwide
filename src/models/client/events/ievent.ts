
import { EventType } from './../../shared/enum/eventtype';
import { EventCategoryType } from './../../shared/enum/eventcategorytype';

export interface IEvent {
	Id: string;
	Image: string;
	Title: string;
	Description: string;
	Link: string;
	EventType: EventType;
	EventCategoryType: EventCategoryType;
	CountryId: number[];
}