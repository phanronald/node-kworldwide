export interface IColor {
	Id: string;
	ColorId: number;
	ColorName: string;
	ImageUrl: string;
}