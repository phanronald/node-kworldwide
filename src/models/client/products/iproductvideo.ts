export interface IProductVideo {
	Id: string;
	ProductId: number;
	FileName: string;
	ThumbnailName: string;
	Title: string;
}
