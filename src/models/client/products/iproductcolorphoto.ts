


export interface IProductColorPhoto {
	Id: string;
	ProductColorPhotoId: number;
	ProductId: number;
	ColorId: number;
	ImageUrl: string;
}
