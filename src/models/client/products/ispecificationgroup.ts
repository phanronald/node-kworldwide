export interface ISpecificationGroup {
	Id: string;
	SpecGroupId: number;
	Name: string;
	SortOrder: number;
}