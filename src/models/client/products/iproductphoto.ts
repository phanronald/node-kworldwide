export interface IProductPhoto {
	Id: string;
	ProductId: number;
	FileName: string;
	Title: string;
}
