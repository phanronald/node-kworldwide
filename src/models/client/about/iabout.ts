
import { IBasicContact } from './../../shared/IBasicContact';
import { ILink } from './../../shared/ILink';

export interface IAbout {
	Id: string;
	HeaderText: string;
	HeaderImage: string;
	Overview: string;
	Detailed: string;
	Contact: IBasicContact;
	MainAddress: string;
	MailAddress: string;
	Phone: string;
	AboutLinks: ILink[];
}