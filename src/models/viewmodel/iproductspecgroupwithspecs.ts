
export interface IProductSpecGroupWithSpecs {
	SpecGroupId: number;
	SpecName: string;
	SpecValue: string;
}