
import { ISpecificationGroup } from './../client/products/ispecificationgroup';
import { IProductSpecGroupWithSpecs } from './iproductspecgroupwithspecs';
import { Collection } from './../../shared/core/collection';

export interface IVmProductSpecification {
	ProductSpecGroupWithSpecs: Collection<IProductSpecGroupWithSpecs>;
	SpecificationGroups: Collection<ISpecificationGroup>;
}
